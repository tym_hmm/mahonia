go 符串编码转换
=======


install
-------

	go get gitee.com/tym_hmm/mahonia

example
-------

	package main
	import "fmt"
	import "gitee.com/tym_hmm/mahonia"
	func main(){
	  enc:=mahonia.NewEncoder("gbk")
	  //converts a  string from UTF-8 to gbk encoding.
	  fmt.Println(enc.ConvertString("hello,世界"))  
	}

donate
-------

